<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>ASKoverflow</title>
  <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
  
  <div id="app">
    <v-app>
    <v-content>
      <v-container>
        <app-index></app-index>
      </v-container>
    </v-content>
  </v-app>
  </div>

  <script src="{{asset('js/app.js')}}"></script>
</body>
</html>